<?php
	
	namespace Zombicide\Map;
	
	
	use App\Game\Board\Unit;
	use MathPHP\LinearAlgebra\Vector;
	use Zombicide\Utility\UnitVector;
	use Zombicide\VectorInterface;
	use Zombicide\VectorTrait;
	
	class Tile implements VectorInterface {
		
		use VectorTrait;
		
		const CELLS = 6;
		const PIXELS = 13;
		const STEP = 0.5;
		
		protected $name;
		protected $rotation;
		protected $pixels;
		
		/** @var  Map */
		protected $map;
		
		protected $cells;
		
		/**
		 * Tile constructor.
		 * @param string                        $name
		 * @param int                           $rotation
		 * @param \MathPHP\LinearAlgebra\Vector $vector
		 */
		public function __construct(string $name, int $rotation, Vector $vector) {
			$this->name = $name;
			$this->rotation = $rotation;
			$this->setVector($vector);
		}
		
		public function getFileContents() {
			$file = __DIR__ . DS . "Tiles" . DS . $this->name . ".json";
			$contents = json_decode(file_get_contents($file));
			for ($i = 0; $i < ($this->rotation / 90) % 4; $i++) {
				$contents = call_user_func_array(
					'array_map',
					array (-1 => null) + array_reverse($contents)
				);
			}
			$contents = array_map(null, ...$contents);
			return $contents;
		}
		
		public function getPixels() {
			if ($this->pixels === null) {
				$contents = $this->getFileContents();
				$vector = $this->getCellVector();
				for ($x = -self::STEP; $x < self::CELLS; $x += self::STEP) {
					for ($y = -self::STEP; $y < self::CELLS; $y += self::STEP) {
						$this->pixels[(string)$vector->add(new Vector([$x, $y]))] = $contents[$x * 2 + 1][$y * 2 + 1];
					}
				}
			}
			return $this->pixels;
		}
		
		public function setPixel(Vector $vector, string $pixel) {
			if (!isset($this->getPixels()[(string)$vector])) {
				throw new \InvalidArgumentException("no pixel found at $vector");
			}
			$this->pixels[(string)$vector] = $pixel;
		}
		
		public function getPixel(Vector $vector): string {
			if (!isset($this->getPixels()[(string)$vector])) {
				throw new \InvalidArgumentException("pixel not found at $vector");
			}
			return $this->getPixels()[(string)$vector];
		}
		
		public function addCell(Cell $cell) {
			if (!$cell->isValid()) {
				return false;
			}
			$this->cells[(string)$cell] = $cell;
			return $cell;
		}
		
		/**
		 * @param \MathPHP\LinearAlgebra\Vector $vector : the vector relative to the tile.
		 * @return \Zombicide\Map\Cell|false
		 */
		public function getCell(Vector $vector) {
			if (!isset($this->cells[(string)$vector])) {
				$cell = $this->addCell(new Cell($this, $vector));
				if (!$cell) {
					return false;
				}
			}
			return $this->cells[(string)$vector];
		}
		
		public function getCells() {
			if (count($this->cells) < pow(self::CELLS, 2)) {
				$vector = $this->getCellVector();
				for ($x = $vector[0]; $x < $vector[0] + self::CELLS; $x++) {
					for ($y = $vector[1]; $y < $vector[1] + self::CELLS; $y++) {
						$this->getCell(new Vector([$x, $y]));
					}
				}
			}
			return $this->cells;
		}
		
		/** @return Map */
		public function getMap(): Map {
			return $this->map;
		}
		
		/**  @param Map $map */
		public function setMap(Map $map) {
			$this->map = $map;
		}
		
		/**
		 * @param $direction
		 * @return \Zombicide\Map\Tile|false
		 */
		public function getAdjacentTile($direction) {
			return $this->getMap()->getTile($this->getVector()->add(UnitVector::get($direction)));
		}
		
		/**
		 * @param null $filter
		 * @return Tile[]
		 */
		public function getAdjacentTiles($filter = null) {
			$tiles = [];
			foreach (UnitVector::DIRECTIONS as $direction) {
				$tile = $this->getAdjacentTile($direction);
				if ($tile && (!is_callable($filter) || $filter($tile, $direction))) {
					$tiles[$direction] = $tile;
				}
			}
			return $tiles;
		}
		
		public function getTopEdge() {
			$edge = [];
			$vector = $this->getPixelVector();
			for ($x = $vector[0]; $x <= $vector[0] + self::CELLS; $x += self::STEP) {
				$edge[] = $this->getPixel(new Vector([$x, $vector[1]]));
			}
			return $edge;
		}
		
		public function getRightEdge() {
			$edge = [];
			$vector = $this->getPixelVector();
			for ($y = $vector[1]; $y <= $vector[1] + self::CELLS; $y += self::STEP) {
				$edge[] = $this->getPixel(new Vector([$vector[0] + self::CELLS, $y]));
			}
			return $edge;
		}
		
		public function getBottomEdge() {
			$edge = [];
			$vector = $this->getPixelVector();
			for ($x = $vector[0]; $x <= $vector[0] + self::CELLS; $x += self::STEP) {
				$edge[] = $this->getPixel(new Vector([$x, $vector[1] + self::CELLS]));
			}
			return $edge;
		}
		
		public function getLeftEdge() {
			$edge = [];
			$vector = $this->getPixelVector();
			for ($y = $vector[1]; $y <= $vector[1] + self::CELLS; $y += self::STEP) {
				$edge[] = $this->getPixel(new Vector([$vector[0], $y]));
			}
			return $edge;
		}
		
		public function getEdge($direction) {
			switch ($direction) {
				case UnitVector::TOP:
					return $this->getTopEdge();
				case UnitVector::RIGHT:
					return $this->getRightEdge();
				case UnitVector::BOTTOM:
					return $this->getBottomEdge();
				case UnitVector::LEFT:
					return $this->getLeftEdge();
			}
			throw new \InvalidArgumentException('Invalid argument for direction.');
		}
		
		public function getEdges() {
			$edges = [];
			foreach (UnitVector::DIRECTIONS as $direction) {
				$edges[$direction] = $this->getEdge($direction);
			}
			return $edges;
		}
		
		public function getCellVector(): Vector {
			return Map::tileVectorToCellVector($this->getVector());
		}
		
		public function getPixelVector(): Vector {
			return $this->getCellVector()->add((UnitVector::up()->add(UnitVector::left())->scalarMultiply(self::STEP)));
		}
		
		public function __toString() {
			return (string)$this->getVector();
		}
		
		
	}