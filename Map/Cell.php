<?php
	
	namespace Zombicide\Map;
	
	use Zombicide\Utility\UnitVector;
	use Zombicide\VectorInterface;
	use Zombicide\VectorTrait;
	
	class Cell implements VectorInterface {
		
		use VectorTrait;
		
		/** @var \Zombicide\Map\Tile */
		protected $tile;
		
		/** @var \Zombicide\Map\Zone */
		protected $zone;
		
		public function __construct(Tile $tile, $vector) {
			$this->setTile($tile);
			$this->setVector($vector);
		}
		
		public function isBoundaryCell() {
			foreach (UnitVector::DIRECTIONS as $direction) {
				if (Pixel::isBoundary($this->getBorder($direction))) {
					return true;
				}
			}
			for ($n = 0; $n < 2; $n++) {
				if (in_array($this->getVector()[$n] % Tile::CELLS, [0, Tile::CELLS - 1])) {
					return true;
				}
			}
			return false;
		}
		
		public function isValid(): bool {
			return $this->getValue() && $this->isBoundaryCell();
		}
		
		/**
		 * @return \Zombicide\Map\Tile
		 */
		public function getTile(): Tile {
			return $this->tile;
		}
		
		/**
		 * @param \Zombicide\Map\Tile $tile
		 */
		public function setTile(Tile $tile) {
			$this->tile = $tile;
		}
		
		public function getZone(): Zone {
			if (!$this->zone) {
				$zone = new Zone($this->getTile()->getMap(), $this->getVector());
				$this->setZone($zone);
			}
			return $this->zone;
		}
		
		public function setZone(Zone $zone) {
			$this->zone = $zone;
		}
		
		public function getValue() {
			return $this->getTile()->getPixel($this->getVector());
		}
		
		/**
		 * @param string $direction
		 * @return string
		 */
		public function getBorder($direction): string {
			return $this->getTile()
				->getPixel($this->getVector()->add(UnitVector::get($direction)->scalarMultiply(TILE::STEP)));
		}
		
		/**
		 * @param callable|null $filter ($border, $direction);
		 * @return array
		 */
		public function getBorders(callable $filter = null) {
			$borders = [];
			foreach (UnitVector::DIRECTIONS as $direction) {
				$border = $this->getBorder($direction);
				if (!is_callable($filter) || $filter($border, $direction)) {
					$borders[$direction] = $border;
				}
			}
			return $borders;
		}
		
		public function isCornerCell(): bool {
			$boundaries = count(
				$this->getBorders(
					function($border) {
						return Pixel::isBoundary($border);
					}
				)
			);
			if ($boundaries === 2) {
				return true;
			}
			if ($boundaries !== 0) {
				return false;
			}
			return false;
			//todo add functionality to pick up inside corners
		}
		
		/**
		 * @param $direction
		 * @return Cell|false
		 */
		public function getAdjacentCell($direction) {
			return $this->getTile()->getMap()->getCell($this->getVector()->add(UnitVector::get($direction)));
		}
		
		
		/**
		 * @param callable|null $filter ($cell, $direction)
		 * @return Cell[]
		 */
		public function getAdjacentCells(callable $filter = null) {
			$cells = [];
			foreach (UnitVector::DIRECTIONS as $direction) {
				$cell = $this->getAdjacentCell($direction);
				if ($cell && (!is_callable($filter) || $filter($cell, $direction))) {
					$cells[$direction] = $cell;
				}
			}
			return $cells;
		}
		
		/*public function getPixelVector() {
			return $this->getVector();
			return Map::cellVectorToPixelVector($this->getVector());
		}*/
		
		public function getTileVector() {
			return Map::cellVectorToTileVector($this->getVector());
		}
		
		public function __toString(): string {
			return (string)$this->getVector();
		}
		
		
	}