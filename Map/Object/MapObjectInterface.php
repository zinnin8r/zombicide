<?php
	/**
	 * Created by PhpStorm.
	 * User: zinni
	 * Date: 1/30/2018
	 * Time: 8:01 PM
	 */
	
	namespace Zombicide\Map\Object;
	
	
	use Zombicide\Map\Map;
	
	interface MapObjectInterface {
		
		public function getMap(): Map;
		
		public function setMap(Map $map);
		
	}