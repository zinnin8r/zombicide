<?php
	/**
	 * Created by PhpStorm.
	 * User: zinni
	 * Date: 1/30/2018
	 * Time: 8:01 PM
	 */
	
	namespace Zombicide\Map\Object;
	
	use Zombicide\Map\Map;
	
	/**
	 * Trait MapObjectTrait
	 * @package Zombicide\Map\Object
	 * @mixin \Zombicide\Map\Object\MapObjectInterface
	 */
	trait MapObjectTrait {
		
		/** @var  \Zombicide\Map\Map */
		protected $map;
		
		public function getMap(): Map {
			return $this->map;
		}
		
		public function setMap(Map $map) {
			$this->map = $map;
		}
		
	}