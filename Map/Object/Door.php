<?php
	
	namespace Zombicide\Map\Object;
	
	
	use MathPHP\LinearAlgebra\Vector;
	use Zombicide\Map\Map;
	use Zombicide\Map\Pixel;
	use Zombicide\Utility\Color;
	use Zombicide\VectorInterface;
	use Zombicide\VectorTrait;
	
	class Door implements VectorInterface, MapObjectInterface {
		
		use VectorTrait;
		
		use MapObjectTrait;
		
		const TYPE_NORMAL = 1;
		
		const STATUS_CLOSED = 0;
		const STATUS_OPEN = 1;
		const STATUS_LOCKED = -1;
		
		protected $properties = [
			'color' => Color::RED,
			'status' => self::STATUS_CLOSED
		];
		
		public function __construct(Vector $vector) {
			$this->setVector($vector);
		}
		
		public function getTile() {
			return $this->getMap()->getTile(Map::cellVectorToTileVector($this->getVector()));
		}
		
		public function getStatus() {
			return $this->properties['status'];
		}
		
		public function open() {
			if ($this->properties['status'] !== static::STATUS_CLOSED) {
				return false;
			}
			$this->properties['status'] = static::STATUS_OPEN;
			$this->getTile()->setPixel($this->getVector(), Pixel::DOOR[0]);
			
			//todo trigger spawn
			return true;
		}
		
		public function close() {
			if ($this->properties['status'] !== static::STATUS_OPEN) {
				return false;
			}
			$this->properties['status'] = static::STATUS_CLOSED;
			$this->getTile()->setPixel($this->getVector(), Pixel::WALL[0]);
			return true;
		}
		
		public function unlock() {
			if ($this->properties['status'] !== static::STATUS_LOCKED) {
				return false;
			}
			$this->properties['status'] = static::STATUS_CLOSED;
			return true;
		}
		
		public function getColor() {
			return $this->properties['color'];
		}
		
		
	}