<?php
	
	namespace Zombicide\Map;
	
	use MathPHP\LinearAlgebra\Vector;
	use Zombicide\Map\Object\Door;
	use Zombicide\Map\Object\MapObjectInterface;
	use Zombicide\Utility\UnitVector;
	use function Zombicide\v;
	
	class Map {
		
		/** @var Tile[] */
		protected $tiles;
		
		protected $objects;
		
		/** @var  Door[] */
		protected $doors;
		
		/**
		 * Map constructor.
		 * @param Tile[] $tiles
		 */
		public function __construct($tiles = []) {
			foreach ($tiles as $tile) {
				$this->addTile($tile);
			}
		}
		
		public function isValid() {
			foreach ($this->getTiles() as $tile) {
				$adjacentTiles = $tile->getAdjacentTiles();
				foreach ($adjacentTiles as $direction => $adjacentTile) {
					if ($tile->getEdge($direction) != $adjacentTile->getEdge(UnitVector::rotate($direction, 2))) {
						return false;
					}
				}
			}
			return true;
		}
		
		public function addTile(Tile $tile) {
			$this->tiles[(string)$tile->getVector()] = $tile;
			$tile->setMap($this);
		}
		
		/**
		 * @param Vector $vector
		 * @return Tile|false
		 */
		public function getTile(Vector $vector) {
			if (!isset($this->tiles[(string)$vector])) {
				return false;
			}
			$tile = $this->tiles[(string)$vector];
			if (!$tile instanceof Tile) {
				$tile = new Tile($tile['name'], $tile['rotation'], $vector);
			}
			return $tile;
		}
		
		/**
		 * @return \Zombicide\Map\Tile[]
		 */
		public function getTiles() {
			foreach ($this->tiles as $vector => $tile) {
				if (!$tile instanceof Tile) {
					$this->getTile(v($vector));
				}
			}
			return $this->tiles;
		}
		
		/**
		 * @param Vector $vector
		 * @return bool|\Zombicide\Map\Zone
		 */
		public function getZone(Vector $vector) {
			$cell = $this->getCell($vector);
			if (!$cell) {
				return false;
			}
			return $cell->getZone();
		}
		
		/**
		 * @return \Zombicide\Map\Zone[]
		 */
		public function getZones() {
			$zones = [];
			foreach ($this->getCells() as $cell) {
				$zone = $cell->getZone();
				if ($zone) {
					$zones[(string)$zone] = $zone;
				}
			}
			return $zones;
		}
		
		/**
		 * @param Vector $vector :  the vector relative to the map.
		 * @return Cell|false
		 */
		public function getCell(Vector $vector) {
			$tile = $this->getTile(static::cellVectorToTileVector($vector));
			if (!$tile) {
				return false;
			}
			return $tile->getCell($vector);
		}
		
		/**
		 * @return \Zombicide\Map\Cell[]
		 */
		public function getCells() {
			$cells = [];
			foreach ($this->tiles as $tile) {
				foreach ($tile->getCells() as $cell) {
					$cells += $cell;
				}
			}
			return $cells;
		}
		
		
		/**
		 * @param \Zombicide\Map\Object\Door $door
		 */
		public function addDoor(Door $door) {
			$this->doors[(string)$door->getVector()] = $door;
			$door->setMap($this);
		}
		
		
		/**
		 * @param \MathPHP\LinearAlgebra\Vector $vector
		 * @return \Zombicide\Map\Object\Door|bool
		 */
		public function getDoor(Vector $vector) {
			if (!isset($this->doors[(string)$vector])) {
				return false;
			}
			return $this->doors[(string)$vector];
		}
		
		/**
		 * @return \Zombicide\Map\Object\Door[]
		 */
		public function getDoors() {
			return $this->doors;
		}
		
		/**
		 * @param Vector $vector
		 * @return \MathPHP\LinearAlgebra\Vector
		 */
		static public function cellVectorToPixelVector(Vector $vector): Vector {
			return new Vector(
				array_map(
					function($v) {
						return (2 * $v) + 1 + floor($v / Tile::CELLS);
					},
					$vector->getVector()
				)
			);
		}
		
		/**
		 * @param Vector $vector
		 * @return \MathPHP\LinearAlgebra\Vector
		 */
		static public function cellVectorToTileVector(Vector $vector): Vector {
			return new Vector(
				array_map(
					function($v) use ($vector) {
						return floor(($v) / Tile::CELLS);
					},
					$vector->getVector()
				)
			);
		}
		
		/**
		 * @param Vector $vector
		 * @return \MathPHP\LinearAlgebra\Vector
		 */
		static public function pixelVectorToCellVector(Vector $vector): Vector {
			return new Vector(
				array_map(
					function($v) {
						return ($v - 1 + floor($v / Tile::PIXELS)) / 2;
					},
					$vector->getVector()
				)
			);
		}
		
		/**
		 * @param Vector $vector
		 * @return \MathPHP\LinearAlgebra\Vector
		 */
		static public function tileVectorToCellVector(Vector $vector): Vector {
			return new Vector(
				array_map(
					function($v) {
						return ($v * Tile::CELLS);
					},
					$vector->getVector()
				)
			);
		}
		
		/**
		 * @param Vector $vector
		 * @return \MathPHP\LinearAlgebra\Vector
		 */
		static public function tileVectorToPixelVector(Vector $vector): Vector {
			return $vector->scalarMultiply(Tile::PIXELS);
		}
		
		
	}