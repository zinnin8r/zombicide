<?php
	/**
	 * Created by PhpStorm.
	 * User: zinni
	 * Date: 1/23/2018
	 * Time: 11:45 AM
	 */
	
	namespace Zombicide\Map;
	
	use MathPHP\LinearAlgebra\Vector;
	use Zombicide\Utility\UnitVector;
	use Zombicide\VectorInterface;
	use Zombicide\VectorTrait;
	
	class Zone implements VectorInterface {
		
		use VectorTrait;
		
		/** @var Cell[] */
		protected $cells;
		
		/** @var Map */
		protected $map;
		
		/** @var Tile[] */
		protected $tiles;
		
		const MAX_BOUNDARY_DISTANCE = 2;
		
		public function __construct(Map $map, Vector $vector) {
			$this->setMap($map);
			//$this->setVector($vector);
			
			$cell = $this->getMap()->getCell($vector);
			$this->cells[(string)$cell] = $cell;
			$queue = [$cell];
			/** @var Cell $cell */
			while ($cell = array_shift($queue)) {
				foreach ($cell->getAdjacentCells(
					function(Cell $_cell, $direction) use ($cell) {
						return !Pixel::isBoundary($cell->getBorder(($direction)))
							&& !isset($this->cells[(string)$_cell]);
					}
				) as $_cell) {
					$this->cells[(string)$_cell] = $_cell;
					$_cell->setZone($this);
					$queue[] = $_cell;
				}
			}
		}
		
		public function getVector(): Vector {
			if ($this->vector === null) {
				$this->vector = collection($this->getCells())->min(
					function(Cell $cell) {
						return ($cell->getVector()[0] * 100) + $cell->getVector()[1];
					}
				)->getVector();
			}
			return $this->vector;
		}
		
		public function getAnchorCell() {
			return $this->getMap()->getCell($this->getVector());
		}
		
		/**
		 * @return Cell[]
		 */
		public function getCells() {
			if ($this->cells === null) {
				/** @var Cell $cell */
				$cell = $this->getMap()->getCell($this->vector);
				$this->cells[(string)$cell->getVector()] = $cell;
				$queue = [$cell];
				while ($cell = array_shift($queue)) {
					collection(
						$cell->getAdjacentCells(
							function(Cell $_cell, $direction) use ($cell) {
								return !Pixel::isBoundary($cell->getBorder(($direction)))
									&& !isset($this->cells[(string)$_cell->getVector()]);
							}
						)
					)->each(
						function(Cell $_cell) use (&$queue) {
							$this->cells[(string)$_cell] = $_cell;
							$_cell->setZone($this);
							$queue[] = $_cell;
						}
					);
				}
			}
			return $this->cells;
		}
		
		/**
		 * @param Map $map
		 */
		public function setMap(Map $map) {
			$this->map = $map;
		}
		
		public function getMap(): Map {
			return $this->map;
		}
		
		public function getTiles() {
			if ($this->tiles === null) {
				foreach ($this->getCells() as $cell) {
					$tile = $cell->getTile();
					$this->tiles[(string)$tile] = $tile;
				}
			}
			return $this->tiles;
		}
		
		public function getAdjacentZones($filter = null) {
			$zones = [];
			foreach ($this->getCells() as $cell) {
				foreach ($cell->getBorders(
					function($border) {
						return Pixel::isBoundary($border);
					}
				) as $direction => $border) {
					$vector = $cell->getVector();
					foreach (range(0, self::MAX_BOUNDARY_DISTANCE) as $r) {
						$vector = $vector->add(UnitVector::get($direction));
						$adjacentCell = $this->getMap()->getCell($vector);
						if ($adjacentCell) {
							break;
						}
					}
					if (empty($adjacentCell)) {
						continue;
					}
					
					$border .= $adjacentCell->getBorder(UnitVector::rotate($direction, 2));
					$zone = $adjacentCell->getZone();
					if (!is_callable($filter) || $filter($zone, $border, $direction)) {
						$zones[(string)$zone] = $zone;
					}
				}
			}
			return $zones;
		}
		
		public function getVisibleZones($direction = null) {
			if ($direction === null) {
				$zones = [];
				foreach (UnitVector::DIRECTIONS as $direction) {
					$zones += $this->getVisibleZones($direction);
				}
				return $zones;
			}
			$zones = [];
			$queue = [$this];
			while ($zone = array_shift($queue)) {
				foreach ($zone->getAdjacentZones(
					function($z, $b, $d) use ($direction) {
						return Pixel::isTransparent($b) && $d == $direction;
					}
				) as $zone) {
					$queue[] = $zone;
					$zones[(string)$zone] = $zone;
				}
				foreach ($zone->getAdjacentZones(
					function($z, $b, $d) use ($direction) {
						return Pixel::isTranslucent($b) && $d == $direction;
					}
				) as $zone) {
					$zones[(string)$zone] = $zone;
				}
			}
			return $zones;
		}
		
		public function getAccessibleZones($distance = 1) {
			$zones = [];
			
			$queue[0] = [$this];
			for ($i = 0; $i < $distance; $i++) {
				$queue[$i + 1] = [];
				/** @var Zone $zone */
				foreach ($queue[$i] as $zone) {
					$step = $zone->getAdjacentZones(
						function($z, $b, $d) use ($zones) {
							return !Pixel::isOpaque($b) && !isset($zones[(string)$z]);
						}
					);
					$zones += $step;
					$queue[$i + 1] += $step;
				}
				/*if (empty($queue[$i+1])) {
					break;
				}*/
			}
			unset($zones[(string)$this]);
			return $zones;
		}
		
		public function __toString(): string {
			return (string)$this->getVector();
		}
	}