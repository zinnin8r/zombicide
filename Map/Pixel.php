<?php
	
	namespace Zombicide\Map;
	
	/**
	 * Class Pixel
	 * @package App\Game
	 * @method static bool isStreet(string $pixels);
	 * @method static bool isSewer(string $pixels);
	 * @method static bool isBuilding(string $pixels);
	 * @method static bool isMall(string $pixels);
	 * @method static bool isPrison(string $pixels);
	 * @method static bool isWall(string $pixels);
	 * @method static bool isCrosswalk(string $pixels);
	 * @method static bool isDoor(string $pixels);
	 * @method static bool isBoundary(string $pixels);
	 * @method static bool isInside(string $pixels);
	 * @method static bool isOutside(string $pixels);
	 * @method static bool isTransparent(string $pixels);
	 *
	 */
	class Pixel {
		
		const STREET = 'S';
		const SEWER = 'O';
		const BUILDING = 'B';
		const MALL = 'M';
		const PRISON = 'P';
		
		const SPACE = ' ';
		const WALL = '-|+';
		const CROSSWALK = '.';
		const DOOR = "*";
		
		const BOUNDARY = self::WALL . self::CROSSWALK . self::DOOR;
		const INSIDE = self::BUILDING . self::MALL . self::PRISON;
		const OUTSIDE = self::STREET . self::SEWER . self::CROSSWALK;
		
		const TRANSPARENT = self::CROSSWALK;
		const TRANSLUCENT = self::DOOR;
		const OPAQUE = self::WALL;
		
		public static function __callStatic($name, $arguments) {
			foreach (str_split($arguments[0]) as $char) {
				if (strpos((string)constant("self::" . strtoupper(substr($name, 2))), $char) !== false) {
					return true;
				}
			}
			return false;
		}
		
		static public function isTranslucent(string $pixels) {
			if (self::isTransparent($pixels)) {
				return false;
			}
			foreach (str_split($pixels[0]) as $char) {
				if (strpos(self::TRANSLUCENT, $char) !== false) {
					return true;
				}
			}
		}
		
		static public function isOpaque(string $pixels) {
			if (self::isTranslucent($pixels)) {
				return false;
			}
			foreach (str_split($pixels[0]) as $char) {
				if (strpos(self::OPAQUE, $char) !== false) {
					return true;
				}
			}
		}
		
		
	}