<?php
	
	namespace Zombicide;
	
	use MathPHP\LinearAlgebra\Vector;
	
	interface VectorInterface {
		
		public function getVector(): Vector;
		
		public function setVector(Vector $vector);
		
	}