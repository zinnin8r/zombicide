<?php
	
	namespace Zombicide;
	
	use MathPHP\LinearAlgebra\Vector;
	
	trait VectorTrait {
		
		/**
		 * @var \MathPHP\LinearAlgebra\Vector
		 */
		protected $vector;
		
		public function getVector(): Vector {
			return $this->vector;
		}
		
		/**
		 * @param array|\MathPHP\LinearAlgebra\Vector $vector
		 */
		public function setVector(Vector $vector) {
			$this->vector = $vector;
		}
		
		static public function parseVector($vector) :Vector {
			if (is_string($vector)) {
				$vector = array_map('doubleval', explode(',', substr($vector, 1, -1)));
			}
			if (is_array($vector)) {
				$vector = new Vector($vector);
			}
			return $vector;
		}
		
	}