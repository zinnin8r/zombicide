<?php
	
	namespace Zombicide;
	
	use MathPHP\LinearAlgebra\Vector;
	
	function v($vector): Vector {
		if (is_string($vector)) {
			$vector = array_map('doubleval', explode(',', substr($vector, 1, -1)));
		}
		if (is_array($vector)) {
			$vector = new Vector($vector);
		}
		return $vector;
	}