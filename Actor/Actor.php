<?php
	
	namespace Zombicide\Actor;
	
	use Zombicide\Map\Zone;
	
	abstract class Actor {
		
		/** @var  Zone */
		public $zone;
		
		public $properties;
		
		public function __construct($name) {
			$this->properties = $this->getFileContent($name);
		}
		
		abstract function getFileContent($name);
		
		/**
		 * @return \Zombicide\Map\Zone
		 */
		public function getZone(): Zone {
			return $this->zone;
		}
		
		/**
		 * @param \Zombicide\Map\Zone $zone
		 */
		public function setZone(Zone $zone) {
			$this->zone = $zone;
		}
		
		public function get($prop) {
			if (isset($this->properties[$prop])) {
				return $this->properties[$prop];
			}
			return false;
		}
		
	}