<?php
	
	namespace Zombicide\Utility;
	
	use MathPHP\LinearAlgebra\Vector;
	
	class UnitVector {
		
		const UP = 1;
		const RIGHT = 2;
		const DOWN = 4;
		const LEFT = 8;
		const DIRECTIONS = [
			self::UP,
			self::RIGHT,
			self::DOWN,
			self::LEFT
		];
		const TOP = self::UP;
		const BOTTOM = self::DOWN;
		
		protected static $up;
		protected static $right;
		protected static $down;
		protected static $left;
		
		/**
		 * @param $direction
		 * @return \MathPHP\LinearAlgebra\Vector
		 */
		public static function get($direction): Vector {
			$vector = new Vector([0, 0]);
			if ($direction & self::UP) {
				$vector = $vector->add(self::up());
			}
			if ($direction & self::RIGHT) {
				$vector = $vector->add(self::right());
			}
			if ($direction & self::DOWN) {
				$vector = $vector->add(self::down());
			}
			if ($direction & self::LEFT) {
				$vector = $vector->add(self::left());
			}
			return $vector;
		}
		
		/**
		 * @return \MathPHP\LinearAlgebra\Vector[]
		 */
		static public function all() {
			$vectors = [];
			foreach (self::DIRECTIONS as $direction) {
				$vectors[$direction] = self::get($direction);
			}
			return $vectors;
		}
		
		
		/**
		 * @return \MathPHP\LinearAlgebra\Vector[]
		 */
		static public function each() {
			return self::all();
		}
		
		/**
		 * @return \MathPHP\LinearAlgebra\Vector
		 */
		static public function up(): Vector {
			if (self::$up === null) {
				self::$up = new Vector([0, -1]);
			}
			return self::$up;
		}
		
		/**
		 * @return \MathPHP\LinearAlgebra\Vector
		 */
		static public function right(): Vector {
			if (self::$right === null) {
				self::$right = new Vector([1, 0]);
			}
			return self::$right;
		}
		
		/**
		 * @return \MathPHP\LinearAlgebra\Vector
		 */
		static public function down(): Vector {
			if (self::$down === null) {
				self::$down = new Vector([0, 1]);
			}
			return self::$down;
		}
		
		/**
		 * @return \MathPHP\LinearAlgebra\Vector
		 */
		static public function left(): Vector {
			if (self::$left === null) {
				self::$left = new  Vector([-1, 0]);
			}
			return self::$left;
		}
		
		static public function rotate($direction, $n = 1) {
			$n = ($n % 4) + 4;
			if ($n > 4) {
				$n-= 4;
			}
			$direction = $direction << $n;
			if ($direction > self::LEFT) {
				$direction = $direction >> 4;
			}
			return $direction;
		}
		
		static public function shiftClockwise($direction) {
			$direction = $direction << 1;
			if ($direction > self::LEFT) {
				$direction = $direction >> 4;
			}
			return $direction;
		}
		
		static public function shiftCounterClockwise($direction) {
			$direction = $direction >> 1;
			if ($direction < self::UP) {
				$direction = self::LEFT;
			}
			return $direction;
		}
		
		static public function parseString(string $vector): Vector {
			return new Vector(array_map('doubleval', explode(',', substr($vector, 1, -1))));
		}
		
	}